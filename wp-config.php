<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kucing_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nhAvkZh#$KfKl SaY9[$=v&p>EP6v}EV:Q0@&0J+SF-vhQv_fq7EH*IPE^ H*!CJ');
define('SECURE_AUTH_KEY',  '6_T-:`,s1@=F0Q5%CLDh%)DS]6C]QN3)@V$~^Z*=a,rgPx~MC_?uv,Rso@zC)Y:#');
define('LOGGED_IN_KEY',    'j6@rvjRMr0xdWW13ME8J6)Dxop]9m8KrC=5Bc^ig/&,X3D?@&/QV<%s8XN4wv#K&');
define('NONCE_KEY',        'hMCNcmjTyId#MrDjK7W|$+^9jTVP)Iqpy!I/^nfT@rx|E8k^ZiW:,gZ=W( >}cn,');
define('AUTH_SALT',        'u0+`gmx%5_l&|BFN}7::|fkK[Wg/J.%I*A,p;tH{9@)e{ ;#b|(yZNR/a%%8cbbi');
define('SECURE_AUTH_SALT', 'Afb4N=4#E}=ezEP>xu)$7]Q:N@>skO@ElfacBjo}/UC:JR+oc4nKn4}PG3TF{gFn');
define('LOGGED_IN_SALT',   'c5OJf}az]H&qhn_XR@EVNdJ,=b@nMlhj,El2mX|e0#Dl`J2YE15._X* q6<yThf9');
define('NONCE_SALT',       '9:,Ge6,~4>2fmeb*ph?ag%U(TS 5UoB@rH/=_*QKhy[6Jus}iMW=P?Riq344d.TE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
